from psychsim_agent import PsychSimAgent
from recipes_healthcenter import *
from simulator.decision import *
from psychsim.reward import *


class PSHealthCenterAgent(PsychSimAgent):
    """Defines the states and dynamics for the health center agent"""

    def __init__(self, world, agent_converter, name):
        """
        Creates a PsychSim health center agent with the given name and parameters.
        :param World world: the PsychSim world to which attach the agent.
        :param AgentConverter agent_converter: the correspondence list between PsychSim agent ids and simulation agents.
        :param str name: the name of the agent to be created in PsychSim.
        """
        super(PSHealthCenterAgent, self).__init__(world, agent_converter, name)
        self.up_stream_agents = []  # upstream agents of healthcenter
        self.num_up_stream = None  # number of upstream agents

        self.total_inventory = None
        self.inv_after_received = None  # inventory after receiving shipment from upstream nodes
        self.inv_after_allocation = None  # inventory after allocating to patients

        self.urgent_cases = None  # number of urgent patients
        self.non_urgent_cases = None  # number of non-urgent patients
        self.total_demand = None  # sum of urgent and non-urgent patients

        # self.treat_threshold = None  # used in prefer urgent recipe
        self.temp_urgent = None  # intermediate variables
        self.temp_non_urgent = None  # intermediate variables
        self.urgent_satisfied = None  # number of urgent patients treated
        self.non_urgent_satisfied = None  # number of non-urgent patients treated

        self.order_amount = None

        self.up_to_level = None

        self.lost_urgent = None
        self.backlog = None  # non-urgent
        self.backlog_after_allocation = None  # backlog after allocating to patients

        self.total_on_order = None  # total on-order from all upstream nodes
        self.on_order_dict = {}  # keys = upstream nodes, values = on-order from that upstream node
        self.onorder_after_received = {}  # on-order dictionary after receiving shipment from upstream nodes

        self.order_to = {}  # keys = upstream nodes, values = order amount to that upstream node

        self.ontime_deliv = {}  # keys = upstream nodes, values = on-time delivery rate of that upstream node
        self.trust = {}  # trustworthiness that healthcenter attributes to each of its upstreams (between 0 and 1)

        self.final_coef = {}  # intermediate variables used for calculating trust
        self.sum_coef = None  # intermediate variables used for calculating trust

        self.received_dock = {}  # keeps information about what agent has received (used by cleaning agent in forward
        # planning)
        self.expctd_recieved = {}  # amount of product that agent expects to receive from its upstream now, according
        # to how much it ordered from them and the leadtime

        self.totals = {INV_HI: 400, UP_TO_HI: 400, UP_STR_HI: 3, ORDER_HI: 400, DEM_HI: 400, CUR_DEM_HI: 400}  #
        # upper bounds on variables' values

    def create_state_variables(self):
        """
        create PsychSim state variables
        Each agent has to create its own state variables.
        """
        sim_agent = self.agent_converter.get_sim_agent_from_psychsim_agent(self)
        agent_name = self.ps_agent.name

        # defines properties / parameters
        for u in sim_agent.upstream_nodes:
            self.up_stream_agents.append(self.agent_converter.get_psychsim_agent(u))

        # syntactic sugar: add properties to agents from state key variables
        self.num_up_stream = self.world.defineState(agent_name, NUM_UP_STREAM, int, lo=0, hi=self.totals[UP_STR_HI])
        self.world.setFeature(self.num_up_stream, len(self.up_stream_agents))  # initial value for this feature

        self.total_inventory = self.world.defineState(agent_name, INVENTORY, int, lo=0, hi=self.totals[INV_HI])
        self.world.setFeature(self.total_inventory, sim_agent.inventory_level())

        self.inv_after_received = self.world.defineState(
            agent_name, INV_AFTER_RECEIVED, int, lo=0.0, hi=self.totals[INV_HI])
        self.world.setFeature(self.inv_after_received, 0.0)

        self.inv_after_allocation = self.world.defineState(
            agent_name, INV_AFTER_ALLOCATION, int, lo=0.0, hi=self.totals[INV_HI])
        self.world.setFeature(self.inv_after_allocation, 0.0)

        self.urgent_cases = self.world.defineState(agent_name, URGENT_CASES, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.urgent_cases, sim_agent.urgent)

        self.non_urgent_cases = self.world.defineState(agent_name, NON_URGENT_CASES, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.non_urgent_cases, sim_agent.non_urgent)

        self.total_demand = self.world.defineState(agent_name, TOTAL_DEMAND, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.total_demand, 0)

        self.temp_urgent = self.world.defineState(agent_name, TEMP_URGENT, int, lo=0.0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.temp_urgent, 0.0)

        # self.temp_urgent_initial = self.world.defineState(
        #   agent_name, TEMP_URGENT_INITIAL, int, lo=0.0, hi=self.totals[DEM_HI])
        # self.world.setFeature(TEMP_URGENT_INITIAL, 0.0)
        self.temp_non_urgent = self.world.defineState(agent_name, TEMP_NON_URGENT, int, lo=0.0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.temp_non_urgent, 0.0)

        self.urgent_satisfied = self.world.defineState(agent_name, URGENT_SATISFIED, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.urgent_satisfied, 0)

        self.non_urgent_satisfied = self.world.defineState(
            self.name, NON_URGENT_SATISFIED, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.non_urgent_satisfied, 0)

        self.treat_threshold = self.world.defineState(agent_name, TREAT_THRESHOLD, int, lo=0, hi=self.totals[ORDER_HI])
        self.world.setFeature(self.treat_threshold, sim_agent.treat_threshold)

        self.order_amount = self.world.defineState(agent_name, ORDER_AMOUNT, int, lo=0, hi=self.totals[ORDER_HI])
        self.world.setFeature(self.order_amount, 0)

        self.up_to_level = self.world.defineState(agent_name, UP_TO_LEVEL, int, lo=0, hi=self.totals[UP_TO_HI])
        self.world.setFeature(self.up_to_level, 0)

        self.lost_urgent = self.world.defineState(agent_name, LOST_URGENT, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.lost_urgent, 0)
        self.backlog_after_allocation = self.world.defineState(agent_name, BACKLOG_AFTER_ALLOCATION, int, lo=0,
                                                               hi=self.totals[DEM_HI])
        self.world.setFeature(self.backlog_after_allocation, 0)
        self.backlog = self.world.defineState(agent_name, BACKLOG, int, lo=0, hi=self.totals[DEM_HI])
        self.world.setFeature(self.backlog, sim_agent.backlog_non_urgent)

        self.total_on_order = self.world.defineState(agent_name, TOTAL_ON_ORDER, int, lo=0, hi=self.totals[ORDER_HI])
        self.world.setFeature(self.total_on_order, sum(oo.amount for oo in sim_agent.on_order))

        self.sum_coef = self.world.defineState(agent_name, SUM_COEF, float, lo=0, hi=1)
        self.world.setFeature(self.sum_coef, 1)

        for u in self.up_stream_agents:
            sim_upst = self.agent_converter.get_sim_agent_from_psychsim_agent(u)
            self.on_order_dict[u] = self.world.defineState(
                self.name, ON_ORDER + str(u), int, lo=0, hi=self.totals[ORDER_HI])
            total_agent_onorder = 0
            for oo in sim_agent.on_order:
                if oo.dst is sim_upst:
                    total_agent_onorder += oo.amount
            self.world.setFeature(self.on_order_dict[u], total_agent_onorder)

            self.onorder_after_received[u] = self.world.defineState(
                self.name, ONORDER_AFTER_RECEIVED + str(u), int, lo=0, hi=self.totals[ORDER_HI])
            self.world.setFeature(self.onorder_after_received[u], 0)

            self.order_to[u] = self.world.defineState(
                agent_name, ORDER_TO + str(u), int, lo=0, hi=self.totals[ORDER_HI])
            self.world.setFeature(self.order_to[u], 0)

            self.ontime_deliv[u] = self.world.defineState(agent_name, ON_TIME_DELIV + str(u), float, lo=0, hi=1)
            self.world.setFeature(self.ontime_deliv[u], 1)

            self.trust[u] = self.world.defineState(
                agent_name, TRUST + str(u), float, lo=0, hi=1)
            self.world.setFeature(self.trust[u], 1)

            self.final_coef[u] = self.world.defineState(
                agent_name, FINAL_COEF + str(u), float, lo=0, hi=1)
            self.world.setFeature(self.final_coef[u], 1)

            self.received_dock[u] = self.world.defineState(
                agent_name, RECEIVED_DOCK + str(u), int, lo=0, hi=self.totals[ORDER_HI])
            self.world.setFeature(self.received_dock[u], 0)

            self.expctd_recieved[u] = self.world.defineState(
                agent_name, EXPCTD_RECIEVED + str(u), int, lo=0, hi=self.totals[ORDER_HI])
            self.world.setFeature(self.expctd_recieved[u], 0)

            # define trust network
            # key = self.world.defineRelation(agent_name, u.ps_agent.name, 'trust', float, lo=0.0, hi=1.0)
            # self.world.setFeature(key, 1.0)

    def define_dependencies(self):
        """
        Each agent has to define the dependencies between the state variables.
        Whenever is calculated based on another variable there need to be a dependecy defined for the pair.
        """
        # dependency for HospitalReceiveShipmentRecipe
        self.world.addDependency(self.inv_after_received, self.total_inventory)
        for u in self.up_stream_agents:
            self.world.addDependency(self.inv_after_received, self.received_dock[u])

        for u in self.up_stream_agents:
            self.world.addDependency(self.onorder_after_received[u], self.received_dock[u])
            self.world.addDependency(self.onorder_after_received[u], self.on_order_dict[u])

        # dependencies for allocation recipes
        # self.world.addDependency(self.temp_urgent, self.urgent_cases)
        # self.world.addDependency(self.temp_urgent, self.inv_after_received)
        # self.world.addDependency(self.temp_urgent_initial, self.inv_after_received)
        # self.world.addDependency(self.temp_urgent_initial, self.current_demand)
        # self.world.addDependency(self.temp_urgent, self.temp_urgent_initial)
        # self.world.addDependency(self.urgent_satisfied, self.temp_urgent)
        # self.world.addDependency(self.urgent_satisfied, self.urgent_cases)
        #
        # self.world.addDependency(self.temp_non_urgent, self.non_urgent_cases)
        # self.world.addDependency(self.temp_non_urgent, self.urgent_cases)
        # self.world.addDependency(self.temp_non_urgent, self.inv_after_received)
        # self.world.addDependency(self.temp_non_urgent, self.urgent_satisfied)
        # self.world.addDependency(self.non_urgent_satisfied, self.temp_non_urgent)
        # self.world.addDependency(self.non_urgent_satisfied, self.non_urgent_cases)
        self.world.addDependency(self.total_demand, self.urgent_cases)
        self.world.addDependency(self.total_demand, self.non_urgent_cases)
        self.world.addDependency(self.total_demand, self.backlog)

        self.world.addDependency(self.urgent_satisfied, self.inv_after_received)
        self.world.addDependency(self.non_urgent_satisfied, self.inv_after_received)
        self.world.addDependency(self.non_urgent_satisfied, self.backlog)
        self.world.addDependency(self.non_urgent_satisfied, self.urgent_cases)

        self.world.addDependency(self.temp_urgent, self.urgent_cases)
        self.world.addDependency(self.temp_urgent, self.inv_after_received)
        self.world.addDependency(self.temp_urgent, self.total_demand)
        self.world.addDependency(self.urgent_satisfied, self.temp_urgent)
        self.world.addDependency(self.urgent_satisfied, self.urgent_cases)

        self.world.addDependency(self.temp_non_urgent, self.non_urgent_cases)
        self.world.addDependency(self.temp_non_urgent, self.backlog)
        self.world.addDependency(self.temp_non_urgent, self.inv_after_received)
        self.world.addDependency(self.temp_non_urgent, self.urgent_satisfied)
        self.world.addDependency(self.non_urgent_satisfied, self.temp_non_urgent)
        self.world.addDependency(self.non_urgent_satisfied, self.non_urgent_cases)

        # dependencies for inventory after allocation
        self.world.addDependency(self.inv_after_allocation, self.urgent_satisfied)
        self.world.addDependency(self.inv_after_allocation, self.non_urgent_satisfied)
        self.world.addDependency(self.inv_after_allocation, self.inv_after_received)

        # dependencies for backlog after allocation
        self.world.addDependency(self.backlog_after_allocation, self.backlog)
        self.world.addDependency(self.backlog_after_allocation, self.non_urgent_cases)
        self.world.addDependency(self.backlog_after_allocation, self.non_urgent_satisfied)

        # dependencies for orders
        for u in self.up_stream_agents:
            self.world.addDependency(self.total_on_order, self.onorder_after_received[u])

        self.world.addDependency(self.order_amount, self.up_to_level)
        self.world.addDependency(self.order_amount, self.backlog_after_allocation)
        self.world.addDependency(self.order_amount, self.inv_after_allocation)
        self.world.addDependency(self.order_amount, self.total_on_order)
        for u in self.up_stream_agents:
            self.world.addDependency(self.final_coef[u], self.trust[u])
            self.world.addDependency(self.final_coef[u], self.sum_coef)
            self.world.addDependency(self.order_to[u], self.final_coef[u])
            self.world.addDependency(self.order_to[u], self.order_amount)
            self.world.addDependency(self.order_to[u], self.num_up_stream)

        # dependencies for shortage
        self.world.addDependency(self.lost_urgent, self.urgent_cases)
        self.world.addDependency(self.lost_urgent, self.urgent_satisfied)

        # dependencies for trust update
        for u in self.up_stream_agents:
            self.world.addDependency(self.trust[u], self.ontime_deliv[u])

    def define_state_dynamics(self, available_recipes):
        """
        Each agent has to define the state variable dynamics according to its own recipes.
        :param dict available_recipes: a dictionary containing the different types of recipes available for the agent.
        """
        update_received_inventory = HospitalReceiveShipmentRecipe('rcv_ship')
        shortage_calculation = HospitalShortageCalculationRecipe('short_calc')
        update_allocated_inventory = HospitalIntermediateStateUpdateRecipe('intermid_updt')

        # define the recipes through combination of trust, order, allocation, etc
        recipe_combinations = PsychSimAgent.get_all_recipe_combinations(available_recipes)
        for recipe_comb in recipe_combinations:

            # creates and adds action with name from all recipes in the combination
            action_name = "-".join([recipe.name for recipe in recipe_comb.values()])
            action = self.ps_agent.addAction({'verb': action_name})

            # add trust recipe
            if recipe_comb.has_key(TRUST):
                recipe_comb[TRUST].register_recipe(self, action)

            # add received inventory
            update_received_inventory.register_recipe(self, action)

            # add allocation (treatment) recipe
            if recipe_comb.has_key(ALLOCATION):
                recipe_comb[ALLOCATION].register_recipe(self, action)

            # intermediate update of total inventory using dummy variable
            update_allocated_inventory.register_recipe(self, action)

            # add forecasting demand
            if recipe_comb.has_key(FORECAST_DEMAND):
                recipe_comb[FORECAST_DEMAND].register_recipe(self, action)

            # add up-to-level calculation
            if recipe_comb.has_key(CALCULATE_UP_TO_LEVEL):
                recipe_comb[CALCULATE_UP_TO_LEVEL].register_recipe(self, action)

            # add ordering amount recipe
            if recipe_comb.has_key(ORDER_AMOUNT):
                recipe_comb[ORDER_AMOUNT].register_recipe(self, action)

            # add ordering split recipe
            if recipe_comb.has_key(ORDERING_SPLIT):
                recipe_comb[ORDERING_SPLIT].register_recipe(self, action)

            # calculate the amount of unsatisfied demand
            shortage_calculation.register_recipe(self, action)

    def update_psychsim_from_simulation(self, now):
        """
        Updates the agent's PsychSim state according to the simulation agent state.
        """
        sim_agent = self.agent_converter.get_sim_agent_from_psychsim_agent(self)

        self.world.setFeature(self.total_inventory, sim_agent.inventory_level())
        self.world.setFeature(self.inv_after_received, 0.0)
        self.world.setFeature(self.inv_after_allocation, 0.0)

        self.world.setFeature(self.non_urgent_cases, sim_agent.non_urgent)
        self.world.setFeature(self.urgent_cases, sim_agent.urgent)
        self.world.setFeature(self.backlog, sim_agent.backlog_non_urgent)
        self.world.setFeature(self.total_demand, 0)
        self.world.setFeature(self.treat_threshold, sim_agent.treat_threshold)

        self.world.setFeature(self.temp_urgent, 0.0)
        self.world.setFeature(self.temp_non_urgent, 0.0)

        self.world.setFeature(self.urgent_satisfied, 0)
        self.world.setFeature(self.non_urgent_satisfied, 0)

        self.world.setFeature(self.order_amount, 0)

        # self.world.setFeature(self.last_forecast, sim_agent.last_forecast)
        # self.world.setFeature(self.current_forecast, 0)
        # self.world.setFeature(self.smoothing_coef, self.smoothing_constant)
        # self.world.setFeature(self.forecast_part_one, 0)
        # self.world.setFeature(self.forecast_part_two, 0)

        self.world.setFeature(self.up_to_level, sim_agent.up_to_level)

        self.world.setFeature(self.lost_urgent, 0)

        self.world.setFeature(self.total_on_order, sum(oo.amount for oo in sim_agent.on_order))
        for u in self.up_stream_agents:
            sim_upst = self.agent_converter.get_sim_agent_from_psychsim_agent(u)
            total_agent_onorder = 0
            for oo in sim_agent.on_order:
                if oo.dst is sim_upst:
                    total_agent_onorder += oo.amount
            self.world.setFeature(self.on_order_dict[u], total_agent_onorder)
            self.world.setFeature(self.onorder_after_received[u], 0)
            self.world.setFeature(self.order_to[u], 0)

            self.world.setFeature(self.received_dock[u], 0)
            self.world.setFeature(self.expctd_recieved[u], 0)
            deliv_rate = sim_agent.ontime_deliv_rate[sim_upst]
            self.world.setFeature(self.ontime_deliv[u], deliv_rate)
            # todo is this necessary?
            # self.world.setFeature(self.trust[u], deliv_rate)

    def update_simulation_from_psychsim(self, outcomes):
        """
        Updates the simulation agent decisions according to the PsychSim agent's actions.
        """
        sim_agent = self.agent_converter.get_sim_agent_from_psychsim_agent(self)

        decision_t = TreatDecision()
        # decision_t.urgent = int(round(outcomes[self.urgent_satisfied]))
        # decision_t.non_urgent = int(round(outcomes[self.non_urgent_satisfied]))
        decision_t.urgent = outcomes[self.urgent_satisfied]
        decision_t.non_urgent = outcomes[self.non_urgent_satisfied]
        sim_agent.decisions.append(decision_t)

        # print self.name
        for u, amt in self.order_to.iteritems():
            decision_o = OrderDecision()
            decision_o.upstream = self.agent_converter.get_sim_agent_from_psychsim_agent(u)
            # print decision_o.upstream.id
            decision_o.amount = outcomes[amt]
            # print decision_o.amount
            # decision_o.amount = int(round(outcomes[amt]))
            sim_agent.decisions.append(decision_o)
