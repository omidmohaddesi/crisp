'''
combined recipe of allocation and recipe
allocation occurs before ordering
'''

import sys
from ConfigParser import SafeConfigParser
from argparse import ArgumentParser
import StringIO

from psychsim.pwl import *
from psychsim.reward import *
from psychsim.action import *
from psychsim.world import World,stateKey,actionKey,binaryKey,modelKey,makeFuture
from psychsim.agent import Agent


class PsychSimDecisionMaker(object):
    def __init__(self, turnOrder):
        self.maxRounds = 4
        self.world = World()
        numHospitals = 2
        numSupplies = 2
        treat_threshold = 1
        hospital1 = Agent('H1')
        hospital2 = Agent('H2')
        supplier1 = Agent('S1')
        supplier2 = Agent('S2')
        hospAgts = [hospital1, hospital2]
        suppAgtNames = {'H1': ['S1', 'S2'], 'H2': ['S1', 'S2']}
        totals = {'hInv': 30, 'hUpTo': 30, 'hOnOrder': 30, 'hUpStr': 2, 'hOrderAmt': 50, 'hDem':30}



        # define Hospital agents
        # Player state, actions and parameters common to all Hospital Agents
        for i in range(numHospitals):
            me = hospAgts[i]
            self.world.addAgent(me)
            # Define the State
            # many of these would be linked to states in the  simulation
            #for allocation decsion
            self.world.defineState(me.name,'TotalInventory',int,lo=0,hi=totals['hInv'])
            me.setState('TotalInventory',5.0)
            self.world.defineState(me.name,'UpToLevel',int,lo=0,hi=totals['hUpTo'])
            me.setState('UpToLevel',21)
            self.world.defineState(me.name,'TotalOnOrder',int,lo=0,hi=totals['hOnOrder'])
            me.setState('TotalOnOrder',4)
            self.world.defineState(me.name,'NumUpStream',int,lo=0,hi=totals['hUpStr'])
            me.setState('NumUpStream',len(suppAgtNames[me.name]))
            self.world.defineState(me.name,'AdjNumUpStream',float,lo=0,hi=totals['hUpStr'])
            me.setState('AdjNumUpStream',len(suppAgtNames[me.name]))
            self.world.defineState(me.name, 'OrderAmount', int, lo=0, hi=totals['hOrderAmt'])
            me.setState('OrderAmount',0)
            for j in range(0, len(suppAgtNames[me.name])):
                self.world.defineState(me.name, 'OrderTo'+str(j), int, lo=0, hi=totals['hOrderAmt'])
                me.setState('OrderTo'+str(j), 0)
            # for order decision
            self.world.defineState(me.name,'TotalDemand',int,lo=0,hi=totals['hDem'])
            me.setState('TotalDemand',7)
            self.world.defineState(me.name,'UrgentCases',int,lo=0,hi=totals['hDem'])
            me.setState('UrgentCases',4)
            self.world.defineState(me.name,'NonUrgentCases',int,lo=0,hi=totals['hDem'])
            me.setState('NonUrgentCases',3)
            self.world.defineState(me.name,'UrgentSatisfied',int,lo=0,hi=totals['hDem'])
            me.setState('UrgentSatisfied',0)
            self.world.defineState(me.name,'NonUrgentSatisfied',int,lo=0,hi=totals['hDem'])
            me.setState('NonUrgentSatisfied',0)
            self.world.defineState(me.name, 'Supply', int, lo=0.0, hi=100.0)
            me.setState('Supply', 0)
            self.world.defineState(me.name, 'ScaledUrgent', int, lo=0.0, hi=100.0)
            me.setState('ScaledUrgent', 0.0)
            self.world.defineState(me.name, 'ResInventory', int, lo=0.0, hi=100.0)
            me.setState('ResInventory', 0.0)

            # These dependencies tell PyschSim the order in which to compute variables, for example

            # dependency for order
            self.world.addDependency(stateKey(me.name, 'OrderAmount'), stateKey(me.name, 'ResInventory'))
            for j in range(0, len(suppAgtNames[me.name])):
                self.world.addDependency(stateKey(me.name, 'OrderTo'+str(j)), stateKey(me.name, 'OrderAmount'))
                self.world.addDependency(stateKey(me.name, 'OrderTo'+str(j)), stateKey(me.name, 'AdjNumUpStream'))

            # dependency for allocation
            self.world.addDependency(stateKey(me.name, 'NonUrgentSatisfied'), stateKey(me.name, 'TotalDemand'))
            self.world.addDependency(stateKey(me.name, 'NonUrgentSatisfied'), stateKey(me.name, 'Supply'))
            self.world.addDependency(stateKey(me.name, 'ScaledUrgent'), stateKey(me.name, 'TotalDemand'))
            self.world.addDependency(stateKey(me.name, 'ScaledUrgent'), stateKey(me.name, 'Supply'))
            self.world.addDependency(stateKey(me.name, 'UrgentSatisfied'), stateKey(me.name, 'ScaledUrgent'))
            self.world.addDependency(stateKey(me.name, 'ResInventory'), stateKey(me.name, 'UrgentSatisfied'))
            self.world.addDependency(stateKey(me.name, 'ResInventory'), stateKey(me.name, 'NonUrgentSatisfied'))
            self.world.addDependency(stateKey(me.name, 'TotalInventory'), stateKey(me.name, 'ResInventory'))


            # Recipes --- which in PsychSim we can treat as Actions

            act1 = me.addAction({'verb': 'decide','recipe':'order_by_trust_allocate_proportional'})

            act2 = me.addAction({'verb': 'decide','recipe':'order_equally_allocate_urgent'})

             #define trust network
            for supplier in suppAgtNames[me.name]:
                key = self.world.defineRelation(me.name,supplier,'trust', float, lo=0.0, hi=1.0)
                self.world.setFeature(key,1.0)

            # Parameters
            # Horizon is how far aead agent reasons to determine next action
            me.setHorizon(1)
            # Discount is how much agent discounts future rewards
            me.setAttribute('discount',0.9)

        self.world.setOrder(turnOrder)

        # setting trust values
        self.world.setFeature(binaryKey('H1', 'S1', 'trust'), .7)
        self.world.setFeature(binaryKey('H2', 'S1', 'trust'), .7)

        #reward function
        print "hospital H1 cares more about Urgent"
        hospital1.setReward(maximizeFeature(stateKey(hospital1.name, 'UrgentSatisfied')),4.0)
        hospital1.setReward(maximizeFeature(stateKey(hospital1.name, 'NonUrgentSatisfied')),1.0)
        hospital1.setReward(minimizeFeature(stateKey(hospital1.name, 'TotalInventory')), 1.0)
        print "hospital H2 cares more about NonUrgent because they make more money from NonUrgent"
        hospital2.setReward(maximizeFeature(stateKey(hospital2.name, 'UrgentSatisfied')),1.0)
        hospital2.setReward(maximizeFeature(stateKey(hospital2.name, 'NonUrgentSatisfied')),4.0)
        hospital2.setReward(minimizeFeature(stateKey(hospital2.name, 'TotalInventory')), 1.0)

        for h in hospAgts:
            TotalInventory = stateKey(h.name, 'TotalInventory')
            UpToLevel = stateKey(h.name, 'UpToLevel')
            TotalOnOrder = stateKey(h.name, 'TotalOnOrder')
            NumUpStream = stateKey(h.name, 'NumUpStream')
            AdjNumUpStream = stateKey(h.name, 'AdjNumUpStream')
            OrderAmount = stateKey(h.name, 'OrderAmount')
            OrderTo = []
            for u in range(0, len(suppAgtNames[h.name])):
                OrderTo.append(stateKey(h.name, 'OrderTo' + str(u)))
            TotalDemand = stateKey(h.name, 'TotalDemand')
            UrgentCases = stateKey(h.name, 'UrgentCases')
            NonUrgentCases = stateKey(h.name, 'NonUrgentCases')
            UrgentSatisfied = stateKey(h.name, 'UrgentSatisfied')
            NonUrgentSatisfied = stateKey(h.name, 'NonUrgentSatisfied')
            Supply = stateKey(h.name, 'Supply')
            ScaledUrgent = stateKey(h.name, 'ScaledUrgent')
            ResInventory = stateKey(h.name, 'ResInventory')


            # combined recipe of keep treat_threshold only for urgent and order equally
            atom = Action({'subject': h.name, 'verb': 'decide', 'recipe': 'order_equally_allocate_urgent'})
            #allocation(treatment) dynamics
            tree = makeTree(KeyedMatrix({TotalDemand: KeyedVector({UrgentCases: 1.0, NonUrgentCases: 1.0})}))
            self.world.setDynamics(TotalDemand, atom, tree)

            tree = makeTree({'if': differenceRow(TotalInventory, TotalDemand, treat_threshold),
                             True: setToFeatureMatrix(UrgentSatisfied, UrgentCases),
                             False: {'if': greaterThanRow(TotalInventory, UrgentCases),
                                     True: setToFeatureMatrix(UrgentSatisfied, UrgentCases),
                                     False: {'if': thresholdRow(TotalInventory, 0),
                                             True: setToFeatureMatrix(UrgentSatisfied, TotalInventory),
                                             False: setToConstantMatrix(UrgentSatisfied, 0)}}})
            self.world.setDynamics(UrgentSatisfied, atom, tree)

            tree = makeTree({'if': differenceRow(TotalInventory, TotalDemand, treat_threshold),
                             True: setToFeatureMatrix(NonUrgentSatisfied, NonUrgentCases),
                             False: {'if': differenceRow(TotalInventory, UrgentCases, treat_threshold),
                                     True: multiSetMatrix(NonUrgentSatisfied, {TotalInventory: 1.0, UrgentCases: -1.0,
                                                                               CONSTANT: -1.0 * treat_threshold}),
                                     False: setToConstantMatrix(NonUrgentSatisfied, 0)}})
            self.world.setDynamics(NonUrgentSatisfied, atom, tree)

            # dummy variable for forcing psychsim to used updated inventory when ordering
            tree = makeTree(multiSetMatrix(ResInventory, {TotalInventory: 1.0, NonUrgentSatisfied: -1.0, UrgentSatisfied: -1.0}))
            self.world.setDynamics(ResInventory, atom, tree)

            # split_order_equally
            tree = makeTree(setToFeatureMatrix(TotalInventory, ResInventory))
            self.world.setDynamics(TotalInventory, atom, tree)

            tree = makeTree({'if': multiCompareRow({UpToLevel: 1.0, ResInventory: -1.0, TotalOnOrder: -1.0}),
                             False: setToConstantMatrix(OrderAmount, 0),
                             True: multiSetMatrix(OrderAmount,
                                                  {UpToLevel: 1.0, ResInventory: -1.0, TotalOnOrder: -1.0})})
            self.world.setDynamics(OrderAmount, atom, tree)

            for i in range(0, len(suppAgtNames[h.name])):
                tree = makeTree({'if': thresholdRow(OrderAmount, 0),
                                 True: setToScaledKeyMatrix(OrderTo[i], OrderAmount, 1.0, NumUpStream, .1),
                                 False: setToConstantMatrix(OrderTo[i], 0)})
                self.world.setDynamics(OrderTo[i], atom, tree)

            # combined recipe of up to a threshold give to urgent then proportionally and order by trust
            atom = Action({'subject': h.name, 'verb': 'decide', 'recipe': 'order_by_trust_allocate_proportional'})

            # uses proportional e.g.,  U = U * ((Inv-T)/(N+U) SO CREATE FOLOWING INTERMEDIATE StateKey VALUES
            # for the denominator and numerator e.g., TotalDemand = N+U and Supppy = Inv-T
            # ScaledUrgent for  U * ((Inv-T)/(N+U)
            tree = makeTree(KeyedMatrix({TotalDemand: KeyedVector({UrgentCases: 1.0, NonUrgentCases: 1.0})}))
            self.world.setDynamics(TotalDemand, atom, tree)
            tree = makeTree(setToFeatureMatrix(Supply, TotalInventory, 1.0, -1.0 * treat_threshold))
            self.world.setDynamics(Supply, atom, tree)
            tree = makeTree(setToScaledKeyMatrix(ScaledUrgent, UrgentCases, Supply, TotalDemand, .1))
            self.world.setDynamics(ScaledUrgent, atom, tree)

            # Nonurgent cases
            tree = makeTree({'if': multiCompareRow({NonUrgentCases: 1, UrgentCases: 1, Supply: -1.0}, 0.0),
                             False: setToFeatureMatrix(NonUrgentSatisfied, NonUrgentCases),
                             True: {'if': thresholdRow(Supply, 0),
                                    True: setToScaledKeyMatrix(NonUrgentSatisfied, NonUrgentCases, Supply, TotalDemand, .1),
                                    False: setToConstantMatrix(NonUrgentSatisfied, 0)}})
            self.world.setDynamics(NonUrgentSatisfied, atom, tree)

            # Urgent cases
            tree = makeTree({'if': multiCompareRow({NonUrgentCases: 1, UrgentCases: 1, Supply: -1}, 0.0),
                             False: setToFeatureMatrix(UrgentSatisfied, UrgentCases),
                             True: {'if': thresholdRow(Supply, 0),
                                    True: {'if': multiCompareRow({UrgentCases: 1, ScaledUrgent: -1}, treat_threshold),
                                           True: setToFeatureMatrix(UrgentSatisfied, ScaledUrgent, 1.0,
                                                                    treat_threshold),
                                           False: setToFeatureMatrix(UrgentSatisfied, UrgentCases)},
                                    False: setToMinMatrix(UrgentSatisfied, UrgentCases, TotalInventory)}})
            self.world.setDynamics(UrgentSatisfied, atom, tree)

            # dummy variable for forcing psychsim to used updated inventory when ordering
            tree = makeTree(
                multiSetMatrix(ResInventory, {TotalInventory: 1.0, NonUrgentSatisfied: -1.0, UrgentSatisfied: -1.0}))
            self.world.setDynamics(ResInventory, atom, tree)

            # order dynamics
            tree = makeTree(setToFeatureMatrix(TotalInventory, ResInventory))
            self.world.setDynamics(TotalInventory, atom, tree)

            #split_order_total_offset_by_trust

            tree = makeTree({'if': multiCompareRow({UpToLevel: 1.0, ResInventory: -1.0, TotalOnOrder: -1.0}),
                             False: setToConstantMatrix(OrderAmount, 0),
                             True: multiSetMatrix(OrderAmount,
                                                  {UpToLevel: 1.0, ResInventory: -1.0, TotalOnOrder: -1.0})})
            self.world.setDynamics(OrderAmount, atom, tree)

            # sum up the suppliers using their trust value as a kind of weird way to estimate likelihood of delivery
            # note unlike above recipe of split equall this handles the case of suppliers not being active
            SuppDict = {}
            for s in suppAgtNames[h.name]:
                SuppDict[binaryKey(h.name, s, "trust")] = 1.0
            tree = makeTree(multiSetMatrix(AdjNumUpStream, SuppDict))
            self.world.setDynamics(AdjNumUpStream, atom, tree)

            # so now order but the divisor is the summed trust values (so you end up ordering more than OrderAmount
            # and entirely untrusted suppliers get no order at all

            for i in range(0, len(suppAgtNames[
                                      h.name])):  # this really should loop over supplier names and do so regardless if they are active
                s = suppAgtNames[h.name][i]
                print s
                tree = makeTree({'if': thresholdRow(OrderAmount, 0),
                                 True: {'if': thresholdRow(binaryKey(h.name, s, "trust"), 0.01),
                                        # is this supplier active/trusted at all
                                        True: setToScaledKeyMatrix(OrderTo[i], OrderAmount, 1.0, AdjNumUpStream,
                                                                   .1),
                                        False: setToConstantMatrix(OrderTo[i], 0)},
                                 False: setToConstantMatrix(OrderTo[i], 0)})
                # instead of Order{i} we probably should directly manipulate beliefs of Supplier (see comment above)
                self.world.setDynamics(OrderTo[i], atom, tree)


    def runit(self, Msg):

        print Msg
        print "####### Starting State Below ###################"
        print "Bel_Pr Agent           State_Features"
        print "_____________________________________"
        self.world.printState()
        print "####### Starting State Above ###################"
        print "\n"
        # STACY CHANGED - note they act in parallel so only one step
        for t in range(1):
            # level returns amount of details
            self.world.explain(self.world.step(), level=3)
            # print self.world.step()
            # self.world.state[None].select()
            self.world.printState()
            if self.world.terminated():
                break

# helper fcns for dynamics
# setToScaledMatrix(Val_to_set,Val_to_scale,numerator,denominator,knot_size,1) the fourth argument sets knot spacing


def setToScaledKeyMatrix(SetVar, V, numerator, denominator, inc, i=1.0):  # simple switch function to handle whether numerator is staekey or a constant
    if (isinstance(numerator, int) or isinstance(numerator, float)):
        return setToScaledDenKeyMatrix(SetVar, V, numerator + inc / 2, denominator, inc,
                                       i=1.0)  # STACY ADDED THRESHOLD
    else:
        return setToScaledKeysMatrix(SetVar, V, numerator, denominator, inc, i, inc / 2)  # STACY ADDED THRESHOLD

### This assumes numerator is a python number and denominator is a statekey
def setToScaledDenKeyMatrix(SetVar, V, numerator, denominator, inc,i):  # dummbest way (would want line segments or at least a balanced tree)
    if (i < inc):
        return setToConstantMatrix(SetVar, 0)  # supply insufficient so set N to 0
    else:
        return {'if': multiCompareRow({denominator: i}, threshold=numerator),
                # are scaled cases greater than supply
                True: setToScaledDenKeyMatrix(SetVar, V, numerator, denominator, inc, i - inc),
                # recurse with a reduced proportion
                False: setToFeatureMatrix(SetVar, V, i)}  # Scale Nonurgent by i

### This assumes both numerator and denominator are statekeys
def setToScaledKeysMatrix(SetVar, V, numerator, denominator, inc, i=1.0, threshold=0):  # dummbest way (would want line segments or at least a balanced tree)
    if (i < inc):
        return setToConstantMatrix(SetVar, 0)  # supply insufficient so set N to 0
    else:
        return {'if': multiCompareRow({denominator: i, numerator: -1.0}, threshold),
                # are scaled cases greater than supply
                True: setToScaledKeysMatrix(SetVar, V, numerator, denominator, inc, i - inc, threshold),
                # recurse with a reduced proportion
                False: setToFeatureMatrix(SetVar, V, i)}  # Scale Nonurgent by i

def setToMinMatrix(Key, Ckey1, Ckey2):
    return {'if': differenceRow(Ckey1, Ckey2, 0.0),
            True: setToFeatureMatrix(Key, Ckey2),
            False: setToFeatureMatrix(Key, Ckey1)}

def setToMaxMatrix(Key, Ckey1, Ckey2):
    return {'if': differenceRow(Ckey1, Ckey2, 0.0),
            True: setToFeatureMatrix(Key, Ckey1),
            False: setToFeatureMatrix(Key, Ckey2)}

# this function just illustrates the guts of conditional tests
def multiCompareRow(ScaledKeys, threshold=0.0):  # scales and sums all the keys and
    # print "scaledcompare", ScaledKeys
    return KeyedPlane(KeyedVector(ScaledKeys), threshold)  # then tests if > threshold

def multiSetMatrix(Key, ScaledKeys):  # scales and sums all the keys in ScaledKeys dict and adds offset if CONSTANT in ScaledKeys
    # print ScaledKeys
    return KeyedMatrix({Key: KeyedVector(ScaledKeys)})  # then sets Key which may be in ScaledKeys in which case it is adding to scaled value of Key

turnOrder = (set(['H1', 'H2']),)
supplyAgts = PsychSimDecisionMaker(turnOrder)

# Create configuration file
config = SafeConfigParser()
f = open('simp_hospital.cfg', 'w')
config.write(f)
f.close()

supplyAgts.runit("Hospital")

