""" All allocation and production recipes for psychsim manufacturer  """
from recipe import Recipe
from helper_functions import *
from psychsim.action import Action


class ManufacturerReceivedProductionRecipe(Recipe):
    """Utility recipe for updating inventory using production dock"""

    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_manufacturer.PSManufacturerAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        # update total inventory
        tree = makeTree(
            multiSetMatrix(agent.inv_after_production, {agent.total_inventory: 1.0, agent.production_dock: 1.0}))
        agent.world.setDynamics(agent.inv_after_production, action, tree)

        # deduct from production dock
        tree = makeTree(multiSetMatrix(agent.total_inproduction_after_received,
                                       {agent.total_in_production: 1.0, agent.production_dock: -1.0}))
        agent.world.setDynamics(agent.total_inproduction_after_received, action, tree)


class ManufacturerAllocateEquallyRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_manufacturer.PSManufacturerAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        # samples = get_bivariate_samples(division_func, 0, agent.totals[INV_HI], 0, agent.totals[DN_STR_HI],
        #                                 agent.totals[INV_HI]/10, agent.totals[DN_STR_HI])
        for d in agent.down_stream_agents:
            # tree = makeTree({'if': thresholdRow(agent.inv_after_production, 0),
            #                  True: tree_from_bivariate_samples(agent.temp_allocate_to[d], agent.inv_after_production,
            #                                                    agent.num_down_stream, samples, 0,  agent.totals[INV_HI]/10 - 1, 0, agent.totals[DN_STR_HI]-1),
            #                  False: setToConstantMatrix(agent.temp_allocate_to[d], 0)})
            tree = makeTree({'if': thresholdRow(agent.inv_after_production, 0),
                             True: setToScaledKeyMatrix(agent.temp_allocate_to[d], agent.inv_after_production, 1.0,
                                                        agent.num_down_stream,
                                                        .01),
                             False: setToConstantMatrix(agent.temp_allocate_to[d], 0)})
            agent.world.setDynamics(agent.temp_allocate_to[d], action, tree)
        # to make sure allocation amount is not more than demand
        for d in agent.down_stream_agents:
            tree = makeTree({'if': greaterThanRow(agent.temp_allocate_to[d], agent.backlog_dict[d]),
                             True: setToFeatureMatrix(agent.allocate_to[d], agent.backlog_dict[d]),
                             False: setToFeatureMatrix(agent.allocate_to[d], agent.temp_allocate_to[d])})
            agent.world.setDynamics(agent.allocate_to[d], action, tree)


class ManufacturerAllocateProportionalRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_manufacturer.PSManufacturerAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        update_bl_dict = {}
        for d in agent.down_stream_agents:
            update_bl_dict[agent.backlog_dict[d]] = 1.0
        tree = makeTree(multiSetMatrix(agent.total_backlog, update_bl_dict))
        agent.world.setDynamics(agent.total_backlog, action, tree)
        # sample_1 = get_bivariate_samples(division_func, 0, agent.totals[BL_HI], 0, agent.totals[BL_HI],
        #                                  agent.totals[BL_HI]/10, agent.totals[BL_HI]/10)
        # sample_2 = get_bivariate_samples(multiply_func, 0, agent.totals[INV_HI], 0, 1.1,
        #                                 agent.totals[INV_HI]/10, 110)
        for d in agent.down_stream_agents:
            tree = makeTree({'if': thresholdRow(agent.inv_after_production, 0),
                             True: setToScaledKeyMatrix(agent.temp_allocate_to[d], agent.inv_after_production,
                                                        agent.backlog_dict[d], agent.total_backlog,
                                                        .01),
                             False: setToConstantMatrix(agent.temp_allocate_to[d], 0)})
            agent.world.setDynamics(agent.temp_allocate_to[d], action, tree)
            # tree_1=makeTree(tree_from_bivariate_samples(agent.temp_allocate_initial[d], agent.backlog_dict[d],
            #                                                    agent.total_backlog, sample_1, 0, agent.totals[BL_HI]/10-1,0, agent.totals[BL_HI]/10-1))
            # agent.world.setDynamics(agent.temp_allocate_initial[d], action, tree_1)
            #
            #
            # tree_2 = makeTree({'if': thresholdRow(agent.inv_after_production, 0),
            #                  True: tree_from_bivariate_samples(agent.temp_allocate_to[d], agent.inv_after_production,
            #                                                    agent.temp_allocate_initial[d], sample_2, 0, agent.totals[INV_HI]/10-1, 0, 109),
            #                  False: setToConstantMatrix(agent.temp_allocate_to[d], 0)})
            # agent.world.setDynamics(agent.temp_allocate_to[d], action, tree_2)
        # to make sure allocation amount is not more than demand
        for d in agent.down_stream_agents:
            tree = makeTree({'if': greaterThanRow(agent.temp_allocate_to[d], agent.backlog_dict[d]),
                             True: setToFeatureMatrix(agent.allocate_to[d], agent.backlog_dict[d]),
                             False: setToFeatureMatrix(agent.allocate_to[d], agent.temp_allocate_to[d])})
            agent.world.setDynamics(agent.allocate_to[d], action, tree)


class ManufacturerIntermediateStateUpdateRecipe(Recipe):
    """Utility recipe to aggregate production and allocation policies in Manufacturer agents"""

    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_manufacturer.PSManufacturerAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        # dummy variable for forcing PsychSim to use updated inventory when producing
        update_inv_dict = {agent.inv_after_production: 1.0}
        for d in agent.down_stream_agents:
            update_inv_dict[agent.allocate_to[d]] = -1.0
        tree = makeTree(multiSetMatrix(agent.inv_after_allocation, update_inv_dict))
        agent.world.setDynamics(agent.inv_after_allocation, action, tree)

        # update backlog
        for d in agent.down_stream_agents:
            tree = makeTree(
                multiSetMatrix(agent.backlog_after_allocation[d],
                               {agent.backlog_dict[d]: 1.0, agent.allocate_to[d]: -1.0}))
            agent.world.setDynamics(agent.backlog_after_allocation[d], action, tree)


class ManufacturerDemandForecastRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_manufacturer.PSManufacturerAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        pass
        # samples_1 = get_bivariate_samples(multiply_func, 0, agent.totals[CUR_DEM_HI], 0, 1, agent.totals[CUR_DEM_HI], 100)
        # tree_1 = makeTree(
        #     tree_from_bivariate_samples(agent.forecast_part_one, agent.current_demand, agent.smoothing_coef, samples_1,
        #                                 0, agent.totals[CUR_DEM_HI]-1, 0, 99))
        # agent.world.setDynamics(agent.forecast_part_one, action, tree_1)
        # samples_2 = get_bivariate_samples(multiply_minus_one_func, 0, agent.totals[CUR_DEM_HI], 0, 1, agent.totals[CUR_DEM_HI], 100)
        # tree_2 = makeTree(
        #     tree_from_bivariate_samples(agent.forecast_part_two, agent.last_forecast, agent.smoothing_coef, samples_2,
        #                                 0, agent.totals[CUR_DEM_HI]-1, 0, 99))
        # agent.world.setDynamics(agent.forecast_part_two, action, tree_2)
        # samples_3 = get_bivariate_samples(add_func, 0, agent.totals[CUR_DEM_HI], 0, agent.totals[CUR_DEM_HI], agent.totals[CUR_DEM_HI], agent.totals[CUR_DEM_HI])
        # tree_3 = makeTree(
        #     tree_from_bivariate_samples(agent.current_forecast, agent.forecast_part_one, agent.forecast_part_two,
        #                                 samples_3,
        #                                 0, agent.totals[CUR_DEM_HI]-1, 0, agent.totals[CUR_DEM_HI]-1))
        # agent.world.setDynamics(agent.current_forecast, action, tree_3)


class ManufacturerUpToLevelRecipe(Recipe):
    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_manufacturer.PSManufacturerAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        pass
        # samples = get_bivariate_samples(multiply_func, 0, agent.totals[CUR_DEM_HI], 0, agent.totals[LEAD_TIME_HI], agent.totals[CUR_DEM_HI],
        #                                 agent.totals[LEAD_TIME_HI])
        # tree = makeTree(
        #     tree_from_bivariate_samples(agent.up_to_level, agent.current_forecast, agent.lead_time, samples, 0, agent.totals[CUR_DEM_HI]-1, 0,
        #                                 agent.totals[LEAD_TIME_HI]-1))
        #
        # agent.world.setDynamics(agent.up_to_level, action, tree)


class ManufacturerUpToProductionRecipe(Recipe):

    def __init__(self, name="Recipe", offset=0.0):
        super(ManufacturerUpToProductionRecipe, self).__init__(name)
        self.offset = offset

    def register_recipe(self, agent, action):
        """
        Registers the recipe in the action.
        :param psychsim_agents.psychsim_manufacturer.PSManufacturerAgent agent: the agent for which to register the recipe.
        :param Action action: the action for which the recipe is going to be added.
        """
        update_bl_dict = {}
        for d in agent.down_stream_agents:
            update_bl_dict[agent.backlog_after_allocation[d]] = 1.0
        tree = makeTree(multiSetMatrix(agent.total_backlog_after_allocation, update_bl_dict))
        agent.world.setDynamics(agent.total_backlog_after_allocation, action, tree)

        offset_factor = 1.0 + self.offset
        tree = makeTree({'if': multiCompareRow({agent.up_to_level: offset_factor,
                                                agent.total_backlog_after_allocation: offset_factor,
                                                agent.inv_after_allocation: -offset_factor,
                                                agent.total_inproduction_after_received: -offset_factor}),
                         False: setToConstantMatrix(agent.production_amount, 0),
                         True: {'if': multiCompareRow({agent.up_to_level: offset_factor,
                                                       agent.total_backlog_after_allocation: offset_factor,
                                                       agent.inv_after_allocation: -offset_factor,
                                                       agent.total_inproduction_after_received: -offset_factor,
                                                       agent.available_capacity: -offset_factor}),
                                True: setToFeatureMatrix(agent.production_amount, agent.available_capacity),
                                False: multiSetMatrix(agent.production_amount,
                                                      {agent.up_to_level: offset_factor,
                                                       agent.total_backlog_after_allocation: offset_factor,
                                                       agent.inv_after_allocation: -offset_factor,
                                                       agent.total_inproduction_after_received: -offset_factor})}})
        agent.world.setDynamics(agent.production_amount, action, tree)
