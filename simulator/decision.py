""" decision provide definitions for various types of decisions """


class Decision(object):
    pass


class ProduceDecision(Decision):
    def __init__(self):
        self.amount = int(0)


class AllocateDecision(Decision):
    def __init__(self):
        self.item = None  # which inventory item (Item() object)
        self.downstream_node = None  # allocate to whom
        self.order = None  # which order is being satisfied (Order() object)
        self.amount = int(0)  # the allocation amount


class OrderDecision(Decision):
    def __init__(self):
        self.upstream = None  # order from whom
        self.amount = int(0)  # how much to order


class TreatDecision(Decision):
    def __init__(self):
        self.urgent = int(0)  # how many urgent patients was treated
        self.non_urgent = int(0)  # how many non-urgent patients was treated
