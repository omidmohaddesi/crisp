''' PatientModel provides algorithms to generate patient '''

import numpy as np


class PatientModel(object):
    ''' PatientModel defines the interface of all Patient Models. '''

    def generate_patient(self, now):
        ''' Update the number of patients of each health center. '''
        pass


class NormalDistPatientModel(PatientModel):
    """
    patient demand is normally distributed
    """

    def __init__(self, health_centers):
        # np.random.seed(0)
        self.health_centers = health_centers  # list of healthcenters with this demand model
        self.seed_num = 0  # seed of the random number generator
        np.random.seed(self.seed_num)
        self.urgent_mean = 0  # mean of urgent patient demand
        self.urgent_stdev = 0  # standard deviation of urgent patient demand
        self.non_urgent_mean = 120  # mean of non-urgent patient demand
        self.non_urgent_stdev = 0  # standard deviation of non-urgent patient demand

    def generate_patient(self, now):
        for hc in self.health_centers:
            urgent = np.random.normal(self.urgent_mean, self.urgent_stdev, 1)[0]
            if urgent < 0:
                urgent = 0
            urgent = round(urgent)

            non_urgent = np.random.normal(self.non_urgent_mean,
                                          self.non_urgent_stdev, 1)[0]
            if non_urgent < 0:
                non_urgent = 0
            non_urgent = round(non_urgent)

            hc.receive_patient(urgent, non_urgent, now)


class UniformDistPatientModel(PatientModel):

    def generate_patient(self, now):
        pass


class ConstantPatientModel(PatientModel):

    def __init__(self, health_centers):
        self.urgent = 0
        self.non_urgent = 120
        self.health_centers = health_centers

    def generate_patient(self, now):
        for hc in self.health_centers:
            hc.receive_patient(self.urgent, self.non_urgent, now)


class DifferentPatientModel(PatientModel):
    def __init__(self, health_centers):
        self.urgent_1 = 0
        self.non_urgent_1 = 120
        self.health_center_1 = health_centers[0]
        self.urgent_2 = 0
        self.non_urgent_2 = 120
        self.health_center_2 = health_centers[1]

    def generate_patient(self, now):
        self.health_center_1.receive_patient(self.urgent_1, self.non_urgent_1, now)
        self.health_center_2.receive_patient(self.urgent_2, self.non_urgent_2, now)


class PredefinedPatientModel(PatientModel):

    def __init__(self, health_centers):
        self.health_centers = health_centers

    def generate_patient(self, now):
        for hc in self.health_centers:
            if now < 140:
                hc.receive_patient(10, 50, now)
            else:
                hc.receive_patient(20, 100, now)
