from simulator.distruption import LineShutDownDisruption
from study.simple_psychsim import *


class TrustAllocRecipeNet2(SimulationProfileBase222):
    """
    A simulation profile for testing the effect of distributors performing forward planning vs. not performing
    forward planning.
    """

    def __init__(self, name, dist_planning_idxs, mn_planning_idxs):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param list dist_planning_idxs: a list with the indexes of the distributors with planning capacity.
        :param list mn_planning_idxs: a list with the indexes of the manufacturers with planning capacity.
        """
        self.dist_planning_idxs = dist_planning_idxs
        self.mn_planning_idxs = mn_planning_idxs
        super(TrustAllocRecipeNet2, self).__init__(name)

    def define_agent_connections(self):
        # sets default full connectivity
        super(TrustAllocRecipeNet2, self).define_agent_connections()

        # HC2 is connected only to DS2 and hence DS1 is only connected to HC1.
        self.hc2.upstream_nodes = [self.ds2]
        self.ds1.downstream_nodes = [self.hc1]

    def parameterize_sim_agents(self):
        # default parametrization up-to level for 2X2X2 complete network
        super(TrustAllocRecipeNet2, self).parameterize_sim_agents()

        # different up-to-levels for manufacturers
        self.ds1.up_to_level = 120
        self.ds2.up_to_level = 360

    def add_disruptions(self):
        # manufacturer shutdown with custom period
        manufacturer_shutdown = LineShutDownDisruption(self.simulation, 40)
        manufacturer_shutdown.manufacturer_id = 1
        manufacturer_shutdown.happen_day_1 = 20
        manufacturer_shutdown.end_day_1 = 30
        manufacturer_shutdown.decrease_factor_1 = 0.625
        self.simulation.disruptions.append(manufacturer_shutdown)

    # def parameterize_psychsim_agents(self):
    #     super(TrustAllocRecipeNet2, self).parameterize_psychsim_agents()
    #
    #     # default parameters for all agents
    #     for agent in self.agent_converter.all_psychsim_agents():
    #         agent.ps_agent.setHorizon(12)  # Horizon is how far ahead agent reasons to determine next action

    def add_manufacturers_recipes(self):

        # sets up allocation recipes
        alloc_prop = ManufacturerAllocateProportionalRecipe('alloc_prop')
        alloc_equally = ManufacturerAllocateEquallyRecipe('alloc_eq')
        alloc_recipes_planning = [alloc_prop, alloc_equally]
        alloc_recipes_no_planning = [alloc_equally]

        # gets manufacturers with planning capacity
        planning_mn = []
        for idx in self.mn_planning_idxs:
            planning_mn.append(self.simulation.manufacturers[idx])

        # adds corresponding recipes according to manufacturer's planning capacity
        for mn in self.simulation.manufacturers:

            alloc_recipes = alloc_recipes_no_planning
            if mn in planning_mn:
                alloc_recipes = alloc_recipes_planning

            available_recipes = {
                ALLOCATION: alloc_recipes,
                # FORECAST_DEMAND: [ManufacturerDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [ManufacturerUpToLevelRecipe('calc_up')],
                PRODUCTION_AMOUNT: [ManufacturerUpToProductionRecipe('prod_pro')]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(mn)
            agent.define_state_dynamics(available_recipes)

    def add_distributors_recipes(self):

        # sets up allocation recipes
        alloc_prop = DistributorAllocateProportionalRecipe('alloc_prop')
        alloc_equally = DistributorAllocateEquallyRecipe('alloc_eq')
        alloc_recipes_planning = [alloc_prop, alloc_equally]
        alloc_recipes_no_planning = [alloc_equally]

        # gets distributors with planning capacity
        planning_ds = []
        for idx in self.dist_planning_idxs:
            planning_ds.append(self.simulation.distributors[idx])

        # adds corresponding recipes according to distributor's planning capacity
        for ds in self.simulation.distributors:

            alloc_recipes = alloc_recipes_no_planning
            if ds in planning_ds:
                alloc_recipes = alloc_recipes_planning

            available_recipes = {
                ALLOCATION: alloc_recipes,
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: [DistributorUpToOrderAmountRecipe('1ord_up')],
                ORDERING_SPLIT: [DistributorOrderSplitByTrustRecipe('split_trust')],
                TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.define_state_dynamics(available_recipes)

    def add_health_centers_recipes(self):
        for hc in self.simulation.health_centers:
            # creates list of recipes
            available_recipes = {
                ALLOCATION: [HospitalAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [HospitalDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [HospitalUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: [HospitalUpToOrderAmountRecipe('1ord_up')],
                ORDERING_SPLIT: [HospitalOrderSplitByTrustRecipe('split_trust')],
                TRUST: [HospitalUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }

            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(hc)
            agent.define_state_dynamics(available_recipes)
