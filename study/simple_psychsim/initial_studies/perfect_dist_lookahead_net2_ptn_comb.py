from simulator.distruption import LineShutDownDisruption
from study.simple_psychsim import *


class PerfectDistLookaheadNetPtnComb(SimulationProfileBase222):
    """
    A simulation profile for testing the effect of distributors performing forward planning vs. not performing
    forward planning.
    """

    def __init__(self, name, dist_lookahead_idxs):
        """
        Creates and configures the profile.
        :param str name: the name of this profile (used to create results folder).
        :param list dist_lookahead_idxs: a list with the indexes of the distributors with planning capacity.
        """
        self.dist_lookahead_idxs = dist_lookahead_idxs
        super(PerfectDistLookaheadNetPtnComb, self).__init__(name)
        self.time_periods = 50

    def add_disruptions(self):
        # manufacturer shutdown with custom period
        manufacturer_shutdown = LineShutDownDisruption(self.simulation, 40)
        manufacturer_shutdown.manufacturer_id = 1
        manufacturer_shutdown.happen_day_1 = 20
        manufacturer_shutdown.end_day_1 = 30
        manufacturer_shutdown.decrease_factor_1 = 0.85
        self.simulation.disruptions.append(manufacturer_shutdown)

    def add_patient_model(self):
        """
        Adds one patient model to generate demand at the health centers.
        Default is a constant patient model for all health centers in the network.
        """
        patient_model = ConstantPatientModel(self.simulation.health_centers)
        patient_model.non_urgent = 30
        patient_model.urgent = 90
        self.simulation.patient_model = patient_model

    def add_distributors_recipes(self):

        # sets up ordering recipes
        order_offset = 0.2
        order_up_to = DistributorUpToOrderAmountRecipe('1ord_up')
        order_more = DistributorUpToOrderAmountRecipe('ord+', order_offset)
        order_less = DistributorUpToOrderAmountRecipe('ord-', -order_offset)
        order_amt_recipes_planning = [order_up_to, order_less, order_more]
        order_amt_recipes_no_planning = [order_up_to]

        # gets distributors with planning capacity
        planning_ds = []
        for idx in self.dist_lookahead_idxs:
            planning_ds.append(self.simulation.distributors[idx])

        # adds corresponding recipes
        for ds in self.simulation.distributors:

            order_amt_recipes = order_amt_recipes_no_planning
            if ds in planning_ds:
                order_amt_recipes = order_amt_recipes_planning

            available_recipes = {
                ALLOCATION: [DistributorAllocateProportionalRecipe('alloc_prop')],
                # FORECAST_DEMAND: [DistributorDemandForecastRecipe('dem_fore')],
                # CALCULATE_UP_TO_LEVEL: [DistributorUpToLevelRecipe('calc_up')],
                ORDER_AMOUNT: order_amt_recipes,
                ORDERING_SPLIT: [DistributorOrderSplitEquallyRecipe('split_eq')],
                TRUST: [DistributorUpdateTrustByHistoryRecipe('upd_trust_hist', 0.8)]
            }
            # creates state-action dynamics
            agent = self.agent_converter.get_psychsim_agent(ds)
            agent.define_state_dynamics(available_recipes)
