import sys

import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns

import simulator.simulation as sim
import simulator.agent as agent
import simulator.network as network
import simulator.simulation_runner as sim_runner
import simulator.decision_maker as dmaker
import simulator.patient_model as pmodel
import simulator.distruption as disr

sns.set_context('paper')
sns.set_style("whitegrid")

simulation = sim.Simulation()

agent_builder = agent.AgentBuilder()
agent_builder.lead_time = 2
agent_builder.review_time = 0
agent_builder.cycle_service_level = 0.9
agent_builder.history_preserve_time = 20

mn1 = agent_builder.build("manufacturer")
mn2 = agent_builder.build("manufacturer")

ds1 = agent_builder.build("distributor")
ds2 = agent_builder.build("distributor")
ds3 = agent_builder.build("distributor")

hc1 = agent_builder.build("health_center")
hc2 = agent_builder.build("health_center")
hc3 = agent_builder.build("health_center")
hc4 = agent_builder.build("health_center")
hc5 = agent_builder.build("health_center")

#Network_type1 : Hc1 and Hc2 can get product from manufacture 1 and manufacturer 2 ; ; beta = 3/8

# hc1.upstream_nodes.extend([ds1, ds2])
# hc2.upstream_nodes.extend([ds2])
# ds1.upstream_nodes.extend([mn2])
# ds2.upstream_nodes.extend([mn1, mn2])
# ds1.downstream_nodes.extend([hc1])
# ds2.downstream_nodes.extend([hc1, hc2])
# mn1.downstream_nodes.extend([ds2])
# mn2.downstream_nodes.extend([ds1, ds2])

#Network_type2 : Hc1 only can get product from manufacturer 1 ; beta = 3/4

# hc1.upstream_nodes.extend([ds1])
# hc2.upstream_nodes.extend([ds2])
# ds1.upstream_nodes.extend([mn1])
# ds2.upstream_nodes.extend([mn1, mn2])
# ds1.downstream_nodes.extend([hc1])
# ds2.downstream_nodes.extend([hc2])
# mn1.downstream_nodes.extend([ds1, ds2])
# mn2.downstream_nodes.extend([ds2])


#Network_Complete ; beta = 0.5
hc1.upstream_nodes.extend([ds1, mn1])
hc2.upstream_nodes.extend([ds2])
hc3.upstream_nodes.extend([ds1, ds2, ds3])
hc4.upstream_nodes.extend([ds3])
hc5.upstream_nodes.extend([mn2])

ds1.downstream_nodes.extend([hc1, hc3])
ds2.downstream_nodes.extend([hc2, hc3])
ds3.downstream_nodes.extend([hc3, hc4])

ds1.upstream_nodes.extend([mn1])
ds2.upstream_nodes.extend([mn1, mn2])
ds3.upstream_nodes.extend([mn2])

mn1.downstream_nodes.extend([ds1, ds2, hc1])
mn2.downstream_nodes.extend([ds1, ds2, hc5])

simulation.add_agent(mn1)
simulation.add_agent(mn2)
simulation.add_agent(ds1)
simulation.add_agent(ds2)
simulation.add_agent(ds3)
simulation.add_agent(hc1)
simulation.add_agent(hc2)
simulation.add_agent(hc3)
simulation.add_agent(hc4)
simulation.add_agent(hc5)

num_agents = 10
net = network.Network(num_agents)
info_net = network.Network(num_agents)
for i in range(num_agents):
    for j in range(num_agents):
        net.connectivity[i, j] = 1
        info_net.connectivity[i, j] = 0
simulation.network = net
simulation.info_network = info_net


decision_maker = dmaker.PerAgentDecisionMaker()

hc1_dmaker = dmaker.UrgentFirstHCDecisionMaker(hc1)
decision_maker.add_decision_maker(hc1_dmaker)

hc2_dmaker = dmaker.UrgentFirstHCDecisionMaker(hc2)
decision_maker.add_decision_maker(hc2_dmaker)

hc3_dmaker = dmaker.UrgentFirstHCDecisionMaker(hc3)
decision_maker.add_decision_maker(hc3_dmaker)

hc4_dmaker = dmaker.UrgentFirstHCDecisionMaker(hc4)
decision_maker.add_decision_maker(hc4_dmaker)

hc5_dmaker = dmaker.UrgentFirstHCDecisionMaker(hc5)
decision_maker.add_decision_maker(hc4_dmaker)

ds1_dmaker = dmaker.SimpleDSDecisionMaker(ds1)
decision_maker.add_decision_maker(ds1_dmaker)

ds2_dmaker = dmaker.SimpleDSDecisionMaker(ds2)
decision_maker.add_decision_maker(ds2_dmaker)

mn1_dmaker = dmaker.SimpleMNDecisionMaker(mn1)
decision_maker.add_decision_maker(mn1_dmaker)

mn2_dmaker = dmaker.SimpleMNDecisionMaker(mn2)
decision_maker.add_decision_maker(mn2_dmaker)

patient_model = pmodel.NormalDistPatientModel([hc1, hc2])

# patient_model = pmodel.ConstantPatientModel([hc1, hc2])
simulation.patient_model = patient_model

total_capacity = 2 * ((patient_model.urgent_mean + 0.5 * patient_model.urgent_stdev) + \
                 (patient_model.non_urgent_mean + 0.5 * patient_model.non_urgent_stdev))
#total_capacity = 2*(patient_model.urgent + patient_model.non_urgent)
beta = 0.5
mn1.line_capacity = 20
mn1.capacity = beta * total_capacity
mn1.num_of_lines = int(mn1.capacity/ mn1.line_capacity)
mn1.num_active_lines = mn1.num_of_lines

mn2.line_capacity = 20
mn2.capacity = (1 - beta) * total_capacity
mn2.num_of_lines = int(mn2.capacity / mn2.line_capacity)
mn2.num_active_lines = mn2.num_of_lines

#disruption = disr.RecallDisruption(simulation)
#disruption.defect_day = [ 35, 36, 37, 38, 39, 48, 49, 50, 51, 52]
#disruption.defect_line = [1, 2, 3, 4, 5, 6,7,8,9,10]
#disruption.manufacturer_id = 0
#simulation.disruptions.append(disruption)

runner = sim_runner.SimulationRunner(simulation, decision_maker)


# log_file = open('log_file.json','w')
data_columns = ['time', 'agent', 'item', 'value', 'unit']
data = pd.DataFrame(columns=data_columns)
for i in range(130):
    runner.next_cycle()

    if i > 30:
        data = data.append(pd.DataFrame(hc1.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(hc2.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(ds1.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(ds2.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(mn1.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(mn2.collect_data(i), columns=data_columns))
    # log_file.write(simulation.to_json())
# log_file.close()

data.reset_index()

# print data.to_string()

# plt.xkcd()
data.to_csv('results.csv')
hc1_data = data[data['agent'] == 'hc_4']
print hc1_data
print hc1_data
plt.figure()
sns.tsplot(hc1_data, time='time', condition='item', value='value',
           unit='unit').set_title('Health Center 1')
plt.savefig('hc1.png', dpi=600)

hc2_data = data[data['agent'] == 'hc_5']
plt.figure()
sns.tsplot(hc2_data, time='time', condition='item', value='value',
           unit='unit').set_title('Health Center 2')
plt.savefig('hc2.png', dpi=600)

ds1_data = data[data['agent'] == 'ds_2']
plt.figure()
sns.tsplot(ds1_data, time='time', condition='item',
           value='value', unit='unit').set_title('Distributor 1')
plt.savefig('ds1.png', dpi=600)

ds2_data = data[data['agent'] == 'ds_3']
plt.figure()
sns.tsplot(ds2_data, time='time', condition='item',
           value='value', unit='unit').set_title('Distributor 2')
plt.savefig('ds2.png', dpi=600)

mn1_data = data[data['agent'] == 'mn_0']
plt.figure()
sns.tsplot(mn1_data, time='time', condition='item',
           value='value', unit='unit').set_title('Manufacture 1')
plt.savefig('mn1.png', dpi=600)

mn2_data = data[data['agent'] == 'mn_1']
plt.figure()
sns.tsplot(mn2_data, time='time', condition='item',
           value='value', unit='unit').set_title('Manufacture 2')
plt.savefig('mn2.png', dpi=600)

inv_data = data[data['item'] == 'inventory']
plt.figure()
sns.tsplot(inv_data, time='time', condition='agent',
           value='value', unit='unit').set_title('Inventory')
plt.savefig('inventory.png', dpi=600)
