import sys

import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns

import simulator.simulation as sim
import simulator.agent as agent
import simulator.network as network
import simulator.simulation_runner as sim_runner
import simulator.decision_maker as dmaker
import simulator.patient_model as pmodel
import simulator.distruption as disr

sns.set_context('paper')
sns.set_style("whitegrid")


def run_simulation(second_recall_happen_day):
    simulation = sim.Simulation()

    agent_builder = agent.AgentBuilder()
    agent_builder.lead_time = 2
    agent_builder.review_time = 0
    agent_builder.cycle_service_level = 0.9
    agent_builder.history_preserve_time = 20

    mn1 = agent_builder.build("manufacturer")
    mn1.num_of_lines = 20
    mn1.line_capacity = 20
    mn1.num_active_lines = 20

    mn2 = agent_builder.build("manufacturer")
    mn2.num_of_lines = 20
    mn2.line_capacity = 20
    mn2.num_active_lines = 20

    ds1 = agent_builder.build("distributor")
    ds2 = agent_builder.build("distributor")

    hc1 = agent_builder.build("health_center")
    hc2 = agent_builder.build("health_center")

    hc1.upstream_nodes.extend([ds1, ds2])
    hc2.upstream_nodes.extend([ds2])
    ds1.upstream_nodes.extend([mn2])
    ds1.downstream_nodes.extend([hc1])
    ds2.upstream_nodes.extend([mn1, mn2])
    ds2.downstream_nodes.extend([hc1, hc2])
    mn1.downstream_nodes.extend([ds2])
    mn2.downstream_nodes.extend([ds1, ds2])
    # all to all conection 
    # hc1.upstream_nodes.extend([ds1, ds2])
    # hc2.upstream_nodes.extend([ds1, ds2])
    # ds1.upstream_nodes.extend([mn1, mn2])
    # ds1.downstream_nodes.extend([hc1, hc2])
    # ds2.upstream_nodes.extend([mn1, mn2])
    # ds2.downstream_nodes.extend([hc1, hc2])
    # mn1.downstream_nodes.extend([ds1, ds2])
    # mn2.downstream_nodes.extend([ds1, ds2])

    simulation.add_agent(mn1)
    simulation.add_agent(mn2)
    simulation.add_agent(ds1)
    simulation.add_agent(ds2)
    simulation.add_agent(hc1)
    simulation.add_agent(hc2)

    net = network.Network(6)
    info_net = network.Network(6)
    for i in range(6):
        for j in range(6):
            net.connectivity[i, j] = 1
            info_net.connectivity[i, j] = 0
    simulation.network = net
    simulation.info_network = info_net


    decision_maker = dmaker.PerAgentDecisionMaker()

    hc1_dmaker = dmaker.UrgentFirstHCDecisionMaker(hc1)
    decision_maker.add_decision_maker(hc1_dmaker)

    hc2_dmaker = dmaker.UrgentFirstHCDecisionMaker(hc2)
    decision_maker.add_decision_maker(hc2_dmaker)

    ds1_dmaker = dmaker.SimpleDSDecisionMaker(ds1)
    decision_maker.add_decision_maker(ds1_dmaker)

    ds2_dmaker = dmaker.SimpleDSDecisionMaker(ds2)
    decision_maker.add_decision_maker(ds2_dmaker)

    mn1_dmaker = dmaker.SimpleMNDecisionMaker(mn1)
    decision_maker.add_decision_maker(mn1_dmaker)

    mn2_dmaker = dmaker.SimpleMNDecisionMaker(mn2)
    decision_maker.add_decision_maker(mn2_dmaker)

    patient_model = pmodel.NormalDistPatientModel([hc1, hc2])
    simulation.patient_model = patient_model

    disruption1 = disr.RecallDisruption(simulation)
    disruption1.happen_day = 50
    disruption1.defect_day = []
    disruption1.defect_line = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    disruption1.manufacturer_id = 1
    for i in range(0, 5):
        disruption1.defect_day.append(disruption1.happen_day - i)
    simulation.disruptions.append(disruption1)

    disruption2 = disr.RecallDisruption(simulation)
    disruption2.happen_day = second_recall_happen_day
    disruption1.defect_day = []
    disruption2.defect_day = []
    disruption2.defect_line = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    disruption2.manufacturer_id = 0
    for i in range(0, 5):
        disruption2.defect_day.append(disruption2.happen_day - i)
    simulation.disruptions.append(disruption2)

    runner = sim_runner.SimulationRunner(simulation, decision_maker)


    # log_file = open('log_file.json','w')
    data_columns = ['time', 'agent', 'item', 'value', 'unit']
    data = pd.DataFrame(columns=data_columns)
    for i in range(100):
        runner.next_cycle()

        data = data.append(pd.DataFrame(hc1.collect_data(i), columns=data_columns))
        data = data.append(pd.DataFrame(hc2.collect_data(i), columns=data_columns))
        # data = data.append(pd.DataFrame(ds1.collect_data(i), columns=data_columns))
        # data = data.append(pd.DataFrame(ds2.collect_data(i), columns=data_columns))
        # data = data.append(pd.DataFrame(mn1.collect_data(i), columns=data_columns))
        # data = data.append(pd.DataFrame(mn2.collect_data(i), columns=data_columns))
        # log_file.write(simulation.to_json())
    # log_file.close()
    data.reset_index()

    hc1_data = data[data['agent'] == 'hc_4']
    plt.figure()
    sns.tsplot(hc1_data, time='time', condition='item', value='value',
               unit='unit').set_title('Health Center 1')
    plt.savefig('hc1_' + str(second_recall_happen_day) + '.png', dpi=600)

    hc2_data = data[data['agent'] == 'hc_5']
    plt.figure()
    sns.tsplot(hc2_data, time='time', condition='item', value='value',
               unit='unit').set_title('Health Center 2')
    plt.savefig('hc2_' + str(second_recall_happen_day) + '.png', dpi=600)


    dataPoint = data.groupby(['item']).get_group('lost').sum()
    # print dataPoint.describe()
    return dataPoint.value


data_columns = ['param', 'value']
data = pd.DataFrame(columns=data_columns)
for i in range(30, 70):

    value = run_simulation(i)
    print "Day: ", i, value
    data = data.append(pd.DataFrame([[i, value]], columns=data_columns))

data.reset_index()
print data.to_string()
plt.figure()
data.plot(x='param')
plt.savefig('num_defect_day.png', dpi=600)
data.to_csv('explore.csv')


# hc1_data = data[data['agent'] == 'hc_4']
# plt.figure()
# sns.tsplot(hc1_data, time='time', condition='item', value='value',
#            unit='unit').set_title('Health Center 1')
# plt.savefig('hc1.png', dpi=600)
#
# hc2_data = data[data['agent'] == 'hc_5']
# plt.figure()
# sns.tsplot(hc2_data, time='time', condition='item', value='value',
#            unit='unit').set_title('Health Center 2')
# plt.savefig('hc2.png', dpi=600)
#
# ds1_data = data[data['agent'] == 'ds_2']
# plt.figure()
# sns.tsplot(ds1_data, time='time', condition='item',
#            value='value', unit='unit').set_title('Distributor 1')
# plt.savefig('ds1.png', dpi=600)
#
# ds2_data = data[data['agent'] == 'ds_3']
# plt.figure()
# sns.tsplot(ds2_data, time='time', condition='item',
#            value='value', unit='unit').set_title('Distributor 2')
# plt.savefig('ds2.png', dpi=600)
#
# mn1_data = data[data['agent'] == 'mn_0']
# plt.figure()
# sns.tsplot(mn1_data, time='time', condition='item',
#            value='value', unit='unit').set_title('Manufacture 1')
# plt.savefig('mn1.png', dpi=600)
#
# mn2_data = data[data['agent'] == 'mn_1']
# plt.figure()
# sns.tsplot(mn2_data, time='time', condition='item',
#            value='value', unit='unit').set_title('Manufacture 2')
# plt.savefig('mn2.png', dpi=600)
#
# inv_data = data[data['item'] == 'inventory']
# plt.figure()
# sns.tsplot(inv_data, time='time', condition='agent',
#            value='value', unit='unit').set_title('Inventory')
# plt.savefig('inventory.png', dpi=600)